#include <iostream>
#include <stdint.h>
#include <iomanip>
#include <string>

int main()
{
 
    std::string first = "Elijah", second = "Makarov";
    
    std::cout << first + second << "\n";
    std::cout << first.length() << "\n";
    std::cout << second.length() << "\n";

    std::cout << first[0] << "\n" << second[second.length() - 1] << "\n";
    std::cout << first.front() << "\n" << second.back() << "\n";
    return 0;
}

